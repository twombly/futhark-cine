use futhark_bindgen::*;

fn main() {
    build(
        Backend::C,
        "../lib/github.com/zshipko/futhark-cine/cine.fut",
        "cine.rs",
    )
}

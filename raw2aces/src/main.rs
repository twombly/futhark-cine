use image2::*;

pub mod cine {
    include!(concat!(env!("OUT_DIR"), "/cine.rs"));
}

fn main() {
    let args: Vec<String> = std::env::args().skip(1).collect();

    // Initialize Futhark
    let ctx = cine::Context::new().expect("Unable to start Futhark context");

    // Load image
    // let mut config = io::oiio::ImageSpec::empty();
    // config.set_attr("raw:ColorSpace", "ACES");
    // let input = io::oiio::ImageInput::open(&args[0], Some(&config)).expect("Unable to open image");
    // let image: Image<f32, Rgb> = input.read().expect("Invalid image");
    // image.save("out0.exr").unwrap();

    let mut image = Image::<f32, Rgb>::open(&args[0]).expect("Invalid image");

    // Convert to ArrayF32D3
    let arr = cine::ArrayF32D3::new(
        &ctx,
        [image.height() as i64, image.width() as i64, 3],
        &image.data,
    )
    .unwrap();
    let r = ctx.raw2aces(&arr).unwrap();
    r.values(&mut image.data).unwrap();

    image.save("out.exr").unwrap();
}

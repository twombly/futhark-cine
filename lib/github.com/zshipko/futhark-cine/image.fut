import "./kernel"

type rect 'a = {x: a, y: a, width: a, height: a}

let rect x y width height = {x, y, width, height}

module type Number = {
  include numeric

  val to_f64: t -> f64
}

module Pixel(N: Number) = {
  type a = N.t
  type t [c] = [c]a

  -- | Create a pixel with all channels set to `0`
  let empty [c] () : [c]a =
    replicate c (N.f64 0)

  -- | Create a pixel with all channels set to `x`
  let init [c] x : [c]a =
    replicate c x

  -- | Clips values to `(min, max)`
  let clip [c] (px: t[c]) min max =
    map (\x -> N.min max (N.max x min)) px

  let normalize [c] (px: t[c]) old_min old_max new_min new_max =
    map (\x ->
      N.((x - old_min) * ((new_max - new_min) / (old_max - old_min)) + new_min)
    ) px
}

module Image(N: Number) = {
  type a = N.t
  type t [h][w][c] = [h][w][c]a

  module Pixel = Pixel(N)

  let make2d [h][w] (image: t[h][w][1]) : [h][w]a =
    map (map (\p -> p[0])) image

  let from2d [h][w] (image: [h][w]a) : t[h][w][1] =
    map (map (\x -> [x])) image

  -- | Image dimensions
  -- Returns `width`, `height`, `channels`
  let shape [h][w][c] (_:  t [h][w][c]) : [3]i64 =
    [w, h, c]

  -- | Apply `f` to each pixel in `t`
  let map_pixel [h][w][c] f (t: t [h][w][c]) =
    map (map f) t

  -- | Apply `f` to each value as `f64` in `t`
  let map_f [h][w][c] f (t: t [h][w][c]) =
    map_pixel (map (\x -> f (N.to_f64 x) |> N.f64)) t

  -- | Fill image with `px`
  let fill [h][w][c] (t: t [h][w][c]) (px: [c]a) =
    map (\ _ -> px) t

  -- | Create a new image of the given dimensions filled with `init`
  let new (w: i64) (h: i64) (c: i64) (init: a) : t[h][w][c] =
    (replicate h (replicate w (replicate c init)))

  -- | Apply `f` to each row
  let rows f image = map f image

  -- | Crop an image to the given `rect`, discarding pixels
  let crop [h][w][c] (image: t[h][w][c]) (r: rect i64) =
    image[r.y:r.y+r.height, r.x:r.x+r.width]

  -- | Adjust the exposure of an image by `stops`
  let exposure [h][w][c] (image: t[h][w][c]) (stops: f64) =
    let e = 2.0 ** stops in
    map_f (* e) image

  -- | Adjust image gamma
  let gamma [h][w][c] (image: t[h][w][c]) x: t[h][w][c] =
    map_f (** x) image

  -- | Convert to linear gamma
  let gamma_lin image = gamma image 2.2

  -- | Convert to log gamma
  let gamma_log image = gamma image (1.0 / 2.2)

  -- | Convert to grayscale
  let grayscale [h][w][c] (image: t[h][w][c]) : t[h][w][1] =
    map_pixel (\px ->
      [N.f64 (N.to_f64 px[0] * 0.21 + N.to_f64 px[1] * 0.72 + N.to_f64 px[2] * 0.7)])
    image

  -- | 3d cross correlation
  let correlate3d [h][w][c][k] (K: [k][k][c]f64) (image: t[h][w][c]): t[h][w][c] =
    let z = replicate c 0 in
    tabulate_2d h w (\y x ->
      let win = tabulate_2d k k (\kr kc ->
        if y + kr >= h || x + kc >= w then
          replicate c 0
        else
          map2 (\x k -> N.to_f64 x * k) image[y + kr, x + kc] K[kr, kc]
      ) in
      let win = flatten win in
      let win = reduce (map2 (+)) z win in
      let win = map N.f64 win in
      (win :> [c]N.t)
    )

  -- | 2d cross correlation
  let correlate2d [h][w][c][k] (K: [k][k]f64) (image: t[h][w][c]): t[h][w][c] =
    correlate3d (Kernel.expand c K) image

  -- | Convolution with 2d kernel
  let convolve2d [h][w][c][k] (K: [k][k]f64) (image: t[h][w][c]): t[h][w][c] =
    let flip x =
      tabulate_2d k k (\i j -> x[k-i-1, k-j-1])
    in
    correlate2d (flip K) image

  -- | 3d convolution
  let convolve3d [h][w][c][k] (K: [k][k][c]f64) (image: t[h][w][c]): t[h][w][c] =
    let flip x =
      tabulate_2d k k (\i j -> x[k-i-1, k-j-1])
    in
    correlate3d (flip K) image


  -- | Normalize between range
  let normalize [h][w][c] (image: t[h][w][c]) old_min old_max min max : t[h][w][c] =
    map_pixel (\px -> Pixel.normalize px old_min old_max min max) image
}

module F32: Number with t = f32 = {
  type t = f32
  let to_f64 (x: t): f64 = f64.f32 x

  open f32
}


module U16: Number with t = u16 = {
  type t = u16
  let to_f64 (x: t): f64 = f64.u16 x

  open u16
}

module ImageF32 = Image(F32)
module ImageU16 = Image(U16)

entry sobel [w][h][c] (input: ImageF32.t[h][w][c]) : ImageF32.t[h][w][c] =
  ImageF32.convolve2d (Kernel.sobel ()) input


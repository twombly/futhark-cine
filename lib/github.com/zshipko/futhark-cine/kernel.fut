module Kernel = {
  type t[k] = [k][k]f64

  let normalize [k] (K: t[k]): t[k] =
    let sum = reduce (+) 0 (map (reduce (+) 0) K) in
    map(map (\x -> x / sum)) K

  let create (k: i64) (f: i64 -> i64 -> f64) : t[k] =
    tabulate_2d k k f

  let gaussian (k: i64) (std: f64) : t[k] =
    let std2 = std * std in
    let a = 1.0 / (2.0 * f64.pi * std2) in
    let k = create k (\i j ->
      let x = f64.i64 (i * i + j * j) / (2.0 / std2) in
      (a * f64.e) ** (-1.0 * x)
    ) in
    normalize k

  let ( +| ) [k] (a: t[k]) (b: t[k]) : t[k] =
    map2(map2 (+)) a b

  let gaussian_3x3 () = gaussian 3 1.4
  let gaussian_5x5 () = gaussian 5 1.4
  let sobel_x (): t[3] = [
    [1.0, 0.0, -1.0],
    [2.0, 0.0, -2.0],
    [1.0, 0.0, -1.0]
  ]

  let sobel_y () : t[3] = [
    [1.0, 2.0, 1.0],
    [0.0, 0.0, 0.0],
    [-1.0, -2.0, -1.0]
  ]

  let sobel () = sobel_x () +| sobel_y ()

  let expand [k] ch (K: t[k]): [k][k][ch]f64 =
    map (map (replicate ch)) K
}

import "./color"
import "./image"

type point2d 'a = [2]a

type point3d 'a = [3]a

let ipart x = f64.floor x
let round x = ipart (x + 0.5)
let fpart x = x - f64.floor x
let rfpart x = 1 - fpart(x)

let plot [c] 'a (image: *ImageF32.t[][][c]) (color: [c]f32) x y n =
  let image[i64.f64 y, i64.f64 x] = map (\i -> f32.f64 (n * f64.f32 i)) color in
  image

-- Draw a line
entry line [c] (image: *ImageF32.t[][][c]) (a: point2d f64) (b: point2d f64) (color: [c]f32) =
  let x0 = a[0] in
  let x1 = b[0] in
  let y0 = a[1] in
  let y1 = b[1] in
  let dx = f64.abs (x1 - x0) in
  let sx = if x0 < x1 then 1 else -1 in
  let dy = -f64.abs (y1 - y0) in
  let sy = if y0 < y1 then 1 else -1 in
  let error = dx + dy in
  loop (x0, y0, error, image) = (x0, y0, error, image) while x0 != x1 || y0 != y1 do
    let image = plot image color x0 y0 1 in
    if x0 == x1 && y0 == y1 then (x1, y1, error, image)
    else
      let e2 = 2 * error in
      let (error, x0) =
        if e2 >= dy then
          if x0 == x1 then (error, x1)
          else (error + dy, x0 + sx)
        else (error, x0)
     in
    let (error, y0) =
      if e2 <= dx then
        if y0 == y1 then (error, y1)
        else (error + dx, y0 + sy)
      else (error, y0)
    in
    (x0, y0, error, image)

entry circle [c] (image: *ImageF32.t[][][c]) (center: point2d f64) r (color: [c]f32) =
  let xc = center[0] in
  let yc = center[1] in
  let plot_circle (image: *ImageF32.t[][][c]) color x y =
    let image = plot image color (xc+x) (yc+y) 1 in
    let image = plot image color (xc-x) (yc+y) 1 in
    let image = plot image color (xc+x) (yc-y) 1 in
    let image = plot image color (xc-x) (yc-y) 1 in
    let image = plot image color (xc+y) (yc+x) 1 in
    let image = plot image color (xc-y) (yc+x) 1 in
    let image = plot image color (xc+y) (yc-x) 1 in
    let image = plot image color (xc-y) (yc-x) 1 in
    image
  in
  let x = 0 in
  let y = r in
  let d = 3 - (2 * r) in
  let image = plot_circle image color x y in
  loop (x, y, d, image) = (x, y, d, image) while y >= x do
    let x = x + 1 in
    let (y, d) =
      if d > 0 then
        let y = y - 1 in
        let d = d + 4 * (x - y) + 10 in
        (y, d)
      else
        (y, d + 4 * x + 6)
    in
    let image = plot_circle image color x y in
    (x, y, d, image)




-- Draws anti-aliased line using the Xiaolin Wu line algorithm
entry line_aa [c] (image: *ImageF32.t[][][c]) (a: point2d f64) (b: point2d f64) (color: [c]f32) =
  let x0 = a[0] in
  let x1 = b[0] in
  let y0 = a[1] in
  let y1 = b[1] in
  let steep = f64.abs (y1 - y0) > f64.abs (x1 - x0) in
  let (x0, x1, y0, y1) = if steep then (y0, y1, x0, x1) else (x0, x1, y0, y1) in
  let (x0, x1, y0, y1) = if x0 > x1 then (x1, x0, y1, y0) else (x0, x1, y0, y1) in
  let dx = x1 - x0 in
  let dy = y1 - y1 in
  let gradient = if dx == 0 then 1.0 else dy / dx in

  -- first endpoint
  let xend = round x0 in
  let yend = y0 + gradient * (xend - x0) in
  let xgap = rfpart yend in
  let xpxl1 = xend in
  let ypxl1 = ipart yend in
  let image = if steep then
      let image = plot image color ypxl1 xpxl1 (rfpart yend * xgap) in
      plot image color (ypxl1+1) xpxl1 (fpart yend * xgap)
    else
      let image = plot image color xpxl1 ypxl1 (rfpart yend * xgap) in
      plot image color xpxl1 (ypxl1+1) (fpart yend * xgap)
  in
  let intery = yend + gradient in -- y intersection

  -- second endpoint
  let xend = round x1 in
  let yend = y1 + gradient * (xend - x1) in
  let xgap = fpart (x1 + 0.5) in
  let xpxl2 = xend in
  let ypxl2 = ipart yend in
  let image = if steep then
      let image = plot image color ypxl2 xpxl2 (rfpart yend * xgap) in
      plot image color (ypxl2+1) xpxl2 (fpart yend * xgap)
    else
      let image = plot image color xpxl2 ypxl2 (rfpart yend * xgap) in
      plot image color xpxl2 (ypxl2+1) (fpart yend * xgap)
  in
  let (_, _, image) =
    if steep then
      loop (x, intery, image) = (xpxl1 + 1, intery, image) while x < xpxl2 - 1 do
        let image = plot image color (ipart intery) x (rfpart intery) in
        let image = plot image color (ipart intery + 1) x (fpart intery) in
        (x + 1, intery + gradient, image)
    else
      loop (x, intery, image) = (xpxl1 + 1, intery, image) while x < xpxl2 - 1 do
        let image = plot image color x (ipart intery) (rfpart intery) in
        let image = plot image color x (ipart intery + 1) (fpart intery) in
        (x + 1, intery + gradient, image)
  in
  image


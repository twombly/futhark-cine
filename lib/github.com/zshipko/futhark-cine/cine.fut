import "./color"
import "./draw"
import "./image"
import "./kernel"

-- Apply conversion matrix to a single pixel
entry apply_pixel (mat: [3][3]f32) (px: [3]f32): [3]f32 =
  map (\x -> map2 (\m p -> m * p) x px |> reduce (+) 0) mat

-- Apply conversion matrix to each pixel in an image
entry apply_image mat image =
  let f = apply_pixel mat in
  ImageF32.map_pixel f image

entry rgb_to_rgba [h][w] (image: [h][w][3]f32) : [h][w][4]f32 =
  map (map (\px -> concat_to 4 px [1.0])) image

entry rgba_to_rgb [h][w] (image: [h][w][4]f32) : [h][w][3]f32 =
  image[:, :, :3]

entry xyz_to_ap0_pixel (pixel: XYZ) : AP0 =
  apply_pixel (copy xyz_to_ap0') pixel

entry xyz_to_ap0 (image: [][]XYZ) : [][]AP0 =
  apply_image xyz_to_ap0' image

entry ap0_to_xyz_pixel (pixel: AP0) : XYZ =
  apply_pixel (copy ap0_to_xyz') pixel

entry ap0_to_xyz (image: [][]AP0) : [][]XYZ =
  apply_image ap0_to_xyz' image

entry xyz_to_ap1_pixel (pixel: XYZ) : AP1 =
  apply_pixel (copy xyz_to_ap1') pixel

entry xyz_to_ap1 (image: [][]XYZ) : [][]AP1 =
  apply_image xyz_to_ap1' image

entry ap1_to_xyz_pixel (pixel: AP1) : XYZ =
  apply_pixel (copy ap1_to_xyz') pixel

entry ap1_to_xyz (image: [][]AP1) : [][]XYZ =
  apply_image ap1_to_xyz' image

entry srgb_to_xyz_pixel (pixel: SRGB) : XYZ =
  apply_pixel (copy srgb_to_xyz') pixel

entry srgb_to_xyz (image: [][]SRGB) : [][]XYZ =
  apply_image srgb_to_xyz' image

entry xyz_to_srgb_pixel (pixel: XYZ) : SRGB =
  apply_pixel (copy xyz_to_srgb') pixel

entry xyz_to_srgb (image: [][]XYZ) : [][]SRGB =
  apply_image xyz_to_srgb' image

entry srgb_to_rgb_pixel (pixel: SRGB) : RGB =
  map (\e -> e ** (1.0 / 2.2)) pixel

entry srgb_to_rgb (image: [][]SRGB) : [][]RGB =
  map (map (srgb_to_rgb_pixel)) image

entry rgb_to_srgb_pixel (pixel: RGB) : SRGB =
  map (\e -> e ** 2.2) pixel

entry rgb_to_srgb (image: [][]RGB) : [][]SRGB =
  map (map (rgb_to_srgb_pixel)) image

entry srgb_to_ap0 (image: [][]SRGB) : [][]AP0 =
  map (map (\px -> srgb_to_xyz_pixel px |> xyz_to_ap0_pixel)) image

entry srgb_to_ap1 (image: [][]SRGB) : [][]AP1 =
  map (map (\px -> srgb_to_xyz_pixel px |> xyz_to_ap0_pixel)) image

entry xyz_to_rec709 (image: [][]XYZ) : [][]SRGB =
  apply_image xyz_to_rec709' image

entry xyz_to_rec2020 (image: [][]XYZ) : [][]SRGB =
  apply_image xyz_to_rec2020' image

def matmul [n][m][p]
           (A: [n][m]f32) (B: [m][p]f32) : [n][p]f32 =
  map (\A_row ->
    map (\B_col ->
          reduce (+) 0.0 (map2 (*) A_row B_col))
        (transpose B))
  A

-- Chromatic adaptation
entry bradford (xyz_from: XYZ) (xyz_to: XYZ) : [3][3]f32 =
  let ma =  [
    [ 0.8951000,  0.2664000, -0.1614000],
    [-0.7502000,  1.7135000,  0.0367000],
    [ 0.0389000, -0.0685000,  1.0296000]
  ] in
  let ma_1 = [
    [ 0.9869929, -0.1470543,  0.1599627],
    [ 0.4323053,  0.5183603,  0.0492912],
    [-0.0085287,  0.0400428,  0.9684867]
  ] in
  let c = apply_pixel ma xyz_from in
  let c' = apply_pixel ma xyz_to in
  let d = [
    [c'[0] / c[0], 0.0, 0.0],
    [0.0, c'[1] / c[1], 0.0],
    [0.0, 0.0, c'[2] / c[2]]
  ] in
  matmul (matmul ma_1 d) ma

entry d (n: i32) =
  match n
  case 50 -> copy d50
  case 55 -> copy d55
  case 65 -> copy d65
  case 75 -> copy d75
  case _ -> [0, 0, 0]

-- let d60tod65: [3][3]f32 = [
--   [ 0.987224  , -0.00611327, 0.0159533 ],
--   [-0.00759836,  1.00186   , 0.00533002],
--   [ 0.00307257, -0.00509595, 1.08168   ]
-- ]

entry raw2aces image =
  let image = ImageF32.gamma image 1.8 in
  let image = srgb_to_xyz image in
  let image = apply_image (bradford d50 d65) image in
  let image = xyz_to_ap0 image in
  let image = ImageF32.exposure image 1 in
  image

entry raw2aces_rgba image =
  let image = rgba_to_rgb image in
  let image = raw2aces image in
  rgb_to_rgba image

entry draw_line = line
entry draw_line_aa = line_aa
entry sobel image = ImageF32.correlate2d (Kernel.sobel ()) image

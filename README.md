# futhark-cine

A Futhark package for cinema related image processing

## Features

- Conversion between many color representations, including [ACES](https://github.com/ampas/aces-dev), XYZ, Lab, and sRGB
  - ACES conversion matrices are from [here](https://github.com/ampas/aces-dev/blob/dev/transforms/ctl/README-MATRIX.md)
  - Lots of other information is from [brucelindbloom.com](http://www.brucelindbloom.com)
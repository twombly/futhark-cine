src=lib/github.com/zshipko/futhark-cine
BACKEND?=ispc
BACKEND_LIBS?=

.PHONY: lib
lib:
	futhark $(BACKEND) $(src)/cine.fut --library
	$(CC) -fPIC -c -o cine.o $(src)/cine.c
	ar rcs libcine.a cine.o
	$(CC) -o libcine.so -shared cine.o $(BACKEND_LIBS)
	cp $(src)/cine.h ./cine.h

clean:
	rm -f cine.o libcine.* cine.h \
	      $(src)/*.c \
		  $(src)/*.h \
		  $(src)/*.json
